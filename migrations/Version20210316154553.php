<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316154553 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activite (id INT AUTO_INCREMENT NOT NULL, activite VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activite_societe (activite_id INT NOT NULL, societe_id INT NOT NULL, INDEX IDX_53C3FA389B0F88B1 (activite_id), INDEX IDX_53C3FA38FCF77503 (societe_id), PRIMARY KEY(activite_id, societe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, type_adresse_id INT NOT NULL, voie VARCHAR(255) DEFAULT NULL, code_postal VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, siret VARCHAR(255) DEFAULT NULL, INDEX IDX_C35F081650E40A79 (type_adresse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_societe (adresse_id INT NOT NULL, societe_id INT NOT NULL, INDEX IDX_B77B82224DE7DC5C (adresse_id), INDEX IDX_B77B8222FCF77503 (societe_id), PRIMARY KEY(adresse_id, societe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adresse_personne (adresse_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_6FD059854DE7DC5C (adresse_id), INDEX IDX_6FD05985A21BD112 (personne_id), PRIMARY KEY(adresse_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emploi (id INT AUTO_INCREMENT NOT NULL, emploi VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emploi_personne (emploi_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_23388F95EC013E12 (emploi_id), INDEX IDX_23388F95A21BD112 (personne_id), PRIMARY KEY(emploi_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail (id INT AUTO_INCREMENT NOT NULL, type_mail_id INT NOT NULL, adresse_mail VARCHAR(255) DEFAULT NULL, INDEX IDX_5126AC48E05600F4 (type_mail_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail_societe (mail_id INT NOT NULL, societe_id INT NOT NULL, INDEX IDX_C3D8BCD2C8776F01 (mail_id), INDEX IDX_C3D8BCD2FCF77503 (societe_id), PRIMARY KEY(mail_id, societe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail_personne (mail_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_D21908A7C8776F01 (mail_id), INDEX IDX_D21908A7A21BD112 (personne_id), PRIMARY KEY(mail_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, nom_personne VARCHAR(255) NOT NULL, prenom_personne VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne_societe (personne_id INT NOT NULL, societe_id INT NOT NULL, INDEX IDX_C7EAD940A21BD112 (personne_id), INDEX IDX_C7EAD940FCF77503 (societe_id), PRIMARY KEY(personne_id, societe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE societe (id INT AUTO_INCREMENT NOT NULL, nom_societe VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, siren VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tel (id INT AUTO_INCREMENT NOT NULL, type_tel_id INT NOT NULL, numero_tel VARCHAR(255) DEFAULT NULL, INDEX IDX_F037AB0FBB8429E8 (type_tel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tel_societe (tel_id INT NOT NULL, societe_id INT NOT NULL, INDEX IDX_A0EA2EB0985342D0 (tel_id), INDEX IDX_A0EA2EB0FCF77503 (societe_id), PRIMARY KEY(tel_id, societe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tel_personne (tel_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_71C63A41985342D0 (tel_id), INDEX IDX_71C63A41A21BD112 (personne_id), PRIMARY KEY(tel_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_adresse (id INT AUTO_INCREMENT NOT NULL, type_adresse VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_mail (id INT AUTO_INCREMENT NOT NULL, type_mail VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_tel (id INT AUTO_INCREMENT NOT NULL, type_tel VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activite_societe ADD CONSTRAINT FK_53C3FA389B0F88B1 FOREIGN KEY (activite_id) REFERENCES activite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activite_societe ADD CONSTRAINT FK_53C3FA38FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F081650E40A79 FOREIGN KEY (type_adresse_id) REFERENCES type_adresse (id)');
        $this->addSql('ALTER TABLE adresse_societe ADD CONSTRAINT FK_B77B82224DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adresse_societe ADD CONSTRAINT FK_B77B8222FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adresse_personne ADD CONSTRAINT FK_6FD059854DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adresse_personne ADD CONSTRAINT FK_6FD05985A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploi_personne ADD CONSTRAINT FK_23388F95EC013E12 FOREIGN KEY (emploi_id) REFERENCES emploi (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE emploi_personne ADD CONSTRAINT FK_23388F95A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail ADD CONSTRAINT FK_5126AC48E05600F4 FOREIGN KEY (type_mail_id) REFERENCES type_mail (id)');
        $this->addSql('ALTER TABLE mail_societe ADD CONSTRAINT FK_C3D8BCD2C8776F01 FOREIGN KEY (mail_id) REFERENCES mail (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_societe ADD CONSTRAINT FK_C3D8BCD2FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_personne ADD CONSTRAINT FK_D21908A7C8776F01 FOREIGN KEY (mail_id) REFERENCES mail (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mail_personne ADD CONSTRAINT FK_D21908A7A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personne_societe ADD CONSTRAINT FK_C7EAD940A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE personne_societe ADD CONSTRAINT FK_C7EAD940FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tel ADD CONSTRAINT FK_F037AB0FBB8429E8 FOREIGN KEY (type_tel_id) REFERENCES type_tel (id)');
        $this->addSql('ALTER TABLE tel_societe ADD CONSTRAINT FK_A0EA2EB0985342D0 FOREIGN KEY (tel_id) REFERENCES tel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tel_societe ADD CONSTRAINT FK_A0EA2EB0FCF77503 FOREIGN KEY (societe_id) REFERENCES societe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tel_personne ADD CONSTRAINT FK_71C63A41985342D0 FOREIGN KEY (tel_id) REFERENCES tel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tel_personne ADD CONSTRAINT FK_71C63A41A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activite_societe DROP FOREIGN KEY FK_53C3FA389B0F88B1');
        $this->addSql('ALTER TABLE adresse_societe DROP FOREIGN KEY FK_B77B82224DE7DC5C');
        $this->addSql('ALTER TABLE adresse_personne DROP FOREIGN KEY FK_6FD059854DE7DC5C');
        $this->addSql('ALTER TABLE emploi_personne DROP FOREIGN KEY FK_23388F95EC013E12');
        $this->addSql('ALTER TABLE mail_societe DROP FOREIGN KEY FK_C3D8BCD2C8776F01');
        $this->addSql('ALTER TABLE mail_personne DROP FOREIGN KEY FK_D21908A7C8776F01');
        $this->addSql('ALTER TABLE adresse_personne DROP FOREIGN KEY FK_6FD05985A21BD112');
        $this->addSql('ALTER TABLE emploi_personne DROP FOREIGN KEY FK_23388F95A21BD112');
        $this->addSql('ALTER TABLE mail_personne DROP FOREIGN KEY FK_D21908A7A21BD112');
        $this->addSql('ALTER TABLE personne_societe DROP FOREIGN KEY FK_C7EAD940A21BD112');
        $this->addSql('ALTER TABLE tel_personne DROP FOREIGN KEY FK_71C63A41A21BD112');
        $this->addSql('ALTER TABLE activite_societe DROP FOREIGN KEY FK_53C3FA38FCF77503');
        $this->addSql('ALTER TABLE adresse_societe DROP FOREIGN KEY FK_B77B8222FCF77503');
        $this->addSql('ALTER TABLE mail_societe DROP FOREIGN KEY FK_C3D8BCD2FCF77503');
        $this->addSql('ALTER TABLE personne_societe DROP FOREIGN KEY FK_C7EAD940FCF77503');
        $this->addSql('ALTER TABLE tel_societe DROP FOREIGN KEY FK_A0EA2EB0FCF77503');
        $this->addSql('ALTER TABLE tel_societe DROP FOREIGN KEY FK_A0EA2EB0985342D0');
        $this->addSql('ALTER TABLE tel_personne DROP FOREIGN KEY FK_71C63A41985342D0');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F081650E40A79');
        $this->addSql('ALTER TABLE mail DROP FOREIGN KEY FK_5126AC48E05600F4');
        $this->addSql('ALTER TABLE tel DROP FOREIGN KEY FK_F037AB0FBB8429E8');
        $this->addSql('DROP TABLE activite');
        $this->addSql('DROP TABLE activite_societe');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE adresse_societe');
        $this->addSql('DROP TABLE adresse_personne');
        $this->addSql('DROP TABLE emploi');
        $this->addSql('DROP TABLE emploi_personne');
        $this->addSql('DROP TABLE mail');
        $this->addSql('DROP TABLE mail_societe');
        $this->addSql('DROP TABLE mail_personne');
        $this->addSql('DROP TABLE personne');
        $this->addSql('DROP TABLE personne_societe');
        $this->addSql('DROP TABLE societe');
        $this->addSql('DROP TABLE tel');
        $this->addSql('DROP TABLE tel_societe');
        $this->addSql('DROP TABLE tel_personne');
        $this->addSql('DROP TABLE type_adresse');
        $this->addSql('DROP TABLE type_mail');
        $this->addSql('DROP TABLE type_tel');
    }
}
