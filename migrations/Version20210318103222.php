<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210318103222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE VIEW viewEntreprise AS
        SELECT 
            societe.nom_societe AS nom_societe,
            societe.logo AS logo,
            societe.siren AS siren,
            activite.activite AS activite,
            tel.numero_tel AS numero_tel,
            type_tel.type_tel AS type_tel,
            mail.adresse_mail AS adresse_mail,
            type_mail.type_mail AS type_mail,
            adresse.voie AS voie,
            adresse.code_postal AS code_postal,
            adresse.ville AS ville,
            type_adresse.type_adresse AS type_adresse,
            adresse.siret AS siret
        FROM
            (((((((((((societe
            JOIN activite_societe ON ((societe.id = activite_societe.societe_id)))
            JOIN activite ON ((activite_societe.activite_id = activite.id)))
            JOIN tel_societe ON ((societe.id = tel_societe.societe_id)))
            JOIN tel ON ((tel_societe.tel_id = tel.id)))
            JOIN type_tel ON ((tel.type_tel_id = type_tel.id)))
            JOIN mail_societe ON ((societe.id = mail_societe.societe_id)))
            JOIN mail ON ((mail_societe.mail_id = mail.id)))
            JOIN type_mail ON ((mail.type_mail_id = type_mail.id)))
            JOIN adresse_societe ON ((societe.id = adresse_societe.societe_id)))
            JOIN adresse ON ((adresse_societe.adresse_id = adresse.id)))
            JOIN type_adresse ON ((adresse.type_adresse_id = type_adresse.id)))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP VIEW viewEntreprise');
    }
}
