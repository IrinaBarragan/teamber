<?php

namespace App\Entity;

use App\Repository\AdresseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseRepository::class)
 */
class Adresse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $voie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @ORM\ManyToMany(targetEntity=Societe::class, inversedBy="adresses_societe")
     */
    private $adresse_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, inversedBy="adresses_personne")
     */
    private $adresse_personne;

    /**
     * @ORM\ManyToOne(targetEntity=TypeAdresse::class, inversedBy="adresse_type", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeAdresse;

    public function __construct()
    {
        $this->adresse_societe = new ArrayCollection();
        $this->adresse_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoie(): ?string
    {
        return $this->voie;
    }

    public function setVoie(?string $voie): self
    {
        $this->voie = $voie;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->code_postal;
    }

    public function setCodePostal(?string $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * @return Collection|Societe[]
     */
    public function getAdresseSociete(): Collection
    {
        return $this->adresse_societe;
    }

    public function addAdresseSociete(Societe $adresseSociete): self
    {
        if (!$this->adresse_societe->contains($adresseSociete)) {
            $this->adresse_societe[] = $adresseSociete;
        }

        return $this;
    }

    public function removeAdresseSociete(Societe $adresseSociete): self
    {
        $this->adresse_societe->removeElement($adresseSociete);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getAdressePersonne(): Collection
    {
        return $this->adresse_personne;
    }

    public function addAdressePersonne(Personne $adressePersonne): self
    {
        if (!$this->adresse_personne->contains($adressePersonne)) {
            $this->adresse_personne[] = $adressePersonne;
        }

        return $this;
    }

    public function removeAdressePersonne(Personne $adressePersonne): self
    {
        $this->adresse_personne->removeElement($adressePersonne);

        return $this;
    }

    public function getTypeAdresse(): ?TypeAdresse
    {
        return $this->typeAdresse;
    }

    public function setTypeAdresse(?TypeAdresse $typeAdresse): self
    {
        $this->typeAdresse = $typeAdresse;

        return $this;
    }
}
