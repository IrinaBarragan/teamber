<?php

namespace App\Entity;

use App\Repository\TypeAdresseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeAdresseRepository::class)
 */
class TypeAdresse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_adresse;

    /**
     * @ORM\OneToMany(targetEntity=Adresse::class, mappedBy="typeAdresse",cascade={"persist"}))
     */
    private $adresse_type;

    public function __construct()
    {
        $this->adresse_type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeAdresse(): ?string
    {
        return $this->type_adresse;
    }

    public function setTypeAdresse(string $type_adresse): self
    {
        $this->type_adresse = $type_adresse;

        return $this;
    }

    /**
     * @return Collection|Adresse[]
     */
    public function getAdresseType(): Collection
    {
        return $this->adresse_type;
    }

    public function addAdresseType(Adresse $adresseType): self
    {
        if (!$this->adresse_type->contains($adresseType)) {
            $this->adresse_type[] = $adresseType;
            $adresseType->setTypeAdresse($this);
        }

        return $this;
    }

    public function removeAdresseType(Adresse $adresseType): self
    {
        if ($this->adresse_type->removeElement($adresseType)) {
            // set the owning side to null (unless already changed)
            if ($adresseType->getTypeAdresse() === $this) {
                $adresseType->setTypeAdresse(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        if ($this->getAdresseType() != null){
            return $this->getAdresseType();
        } else {
            return "";
        }
    }
}
