<?php

namespace App\Entity;

use App\Repository\SocieteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SocieteRepository::class)
 */
class Societe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_societe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siren;

    /**
     * @ORM\ManyToMany(targetEntity=Activite::class, mappedBy="activite_societe")
     */
    private $activites;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, mappedBy="societe_personne", cascade={"persist"})
     */
    private $personnes_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Adresse::class, mappedBy="adresse_societe")
     */
    private $adresses_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Tel::class, mappedBy="tel_societe")
     */
    private $tels_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Mail::class, mappedBy="mail_societe")
     */
    private $mails_societe;

    public function __construct()
    {
        $this->activites = new ArrayCollection();
        $this->personnes_societe = new ArrayCollection();
        $this->adresses_societe = new ArrayCollection();
        $this->tels_societe = new ArrayCollection();
        $this->mails_societe = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomSociete(): ?string
    {
        return $this->nom_societe;
    }

    public function setNomSociete(string $nom_societe): self
    {
        $this->nom_societe = $nom_societe;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(?string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->addActiviteSociete($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            $activite->removeActiviteSociete($this);
        }

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getPersonnesSociete(): Collection
    {
        return $this->personnes_societe;
    }

    public function addPersonnesSociete(Personne $personnesSociete): self
    {
        if (!$this->personnes_societe->contains($personnesSociete)) {
            $this->personnes_societe[] = $personnesSociete;
            $personnesSociete->addSocietePersonne($this);
        }

        return $this;
    }

    public function removePersonnesSociete(Personne $personnesSociete): self
    {
        if ($this->personnes_societe->removeElement($personnesSociete)) {
            $personnesSociete->removeSocietePersonne($this);
        }

        return $this;
    }

    /**
     * @return Collection|Adresse[]
     */
    public function getAdressesSociete(): Collection
    {
        return $this->adresses_societe;
    }

    public function addAdressesSociete(Adresse $adressesSociete): self
    {
        if (!$this->adresses_societe->contains($adressesSociete)) {
            $this->adresses_societe[] = $adressesSociete;
            $adressesSociete->addAdresseSociete($this);
        }

        return $this;
    }

    public function removeAdressesSociete(Adresse $adressesSociete): self
    {
        if ($this->adresses_societe->removeElement($adressesSociete)) {
            $adressesSociete->removeAdresseSociete($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tel[]
     */
    public function getTelsSociete(): Collection
    {
        return $this->tels_societe;
    }

    public function addTelsSociete(Tel $telsSociete): self
    {
        if (!$this->tels_societe->contains($telsSociete)) {
            $this->tels_societe[] = $telsSociete;
            $telsSociete->addTelSociete($this);
        }

        return $this;
    }

    public function removeTelsSociete(Tel $telsSociete): self
    {
        if ($this->tels_societe->removeElement($telsSociete)) {
            $telsSociete->removeTelSociete($this);
        }

        return $this;
    }

    /**
     * @return Collection|Mail[]
     */
    public function getMailsSociete(): Collection
    {
        return $this->mails_societe;
    }

    public function addMailsSociete(Mail $mailsSociete): self
    {
        if (!$this->mails_societe->contains($mailsSociete)) {
            $this->mails_societe[] = $mailsSociete;
            $mailsSociete->addMailSociete($this);
        }

        return $this;
    }

    public function removeMailsSociete(Mail $mailsSociete): self
    {
        if ($this->mails_societe->removeElement($mailsSociete)) {
            $mailsSociete->removeMailSociete($this);
        }

        return $this;
    }
    public function __toString()
    {
        if ($this->getNomSociete() != null){
            return $this->getNomSociete();
        } else {
            return "";
        }
    }
}
