<?php

namespace App\Entity;

use App\Repository\TypeMailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeMailRepository::class)
 */
class TypeMail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_mail;

    /**
     * @ORM\OneToMany(targetEntity=Mail::class, mappedBy="typeMail", cascade={"persist"})
     */
    private $mail_type;

    public function __construct()
    {
        $this->mail_type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeMail(): ?string
    {
        return $this->type_mail;
    }

    public function setTypeMail(string $type_mail): self
    {
        $this->type_mail = $type_mail;

        return $this;
    }

    /**
     * @return Collection|Mail[]
     */
    public function getMailType(): Collection
    {
        return $this->mail_type;
    }

    public function addMailType(Mail $mailType): self
    {
        if (!$this->mail_type->contains($mailType)) {
            $this->mail_type[] = $mailType;
            $mailType->setTypeMail($this);
        }

        return $this;
    }

    public function removeMailType(Mail $mailType): self
    {
        if ($this->mail_type->removeElement($mailType)) {
            // set the owning side to null (unless already changed)
            if ($mailType->getTypeMail() === $this) {
                $mailType->setTypeMail(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if ($this->getTypeMail() != null){
            return $this->getTypeMail();
        } else {
            return "";
        }
   
    }
}
