<?php

namespace App\Entity;

use App\Repository\TelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\FormTypeInterface;

/**
 * @ORM\Entity(repositoryClass=TelRepository::class)
 */
class Tel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_tel;

    /**
     * @ORM\ManyToMany(targetEntity=Societe::class, inversedBy="tels_societe")
     */
    private $tel_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, inversedBy="tels_personne")
     */
    private $tel_personne;

    /**
     * @ORM\ManyToOne(targetEntity=TypeTel::class, inversedBy="tel_type", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeTel;

    public function __construct()
    {
        $this->tel_societe = new ArrayCollection();
        $this->tel_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroTel(): ?string
    {
        return $this->numero_tel;
    }

    public function setNumeroTel(?string $numero_tel): self
    {
        $this->numero_tel = $numero_tel;

        return $this;
    }

    /**
     * @return Collection|Societe[]
     */
    public function getTelSociete(): Collection
    {
        return $this->tel_societe;
    }

    public function addTelSociete(Societe $telSociete): self
    {
        if (!$this->tel_societe->contains($telSociete)) {
            $this->tel_societe[] = $telSociete;
        }

        return $this;
    }

    public function removeTelSociete(Societe $telSociete): self
    {
        $this->tel_societe->removeElement($telSociete);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getTelPersonne(): Collection
    {
        return $this->tel_personne;
    }

    public function addTelPersonne(Personne $telPersonne): self
    {
        if (!$this->tel_personne->contains($telPersonne)) {
            $this->tel_personne[] = $telPersonne;
        }

        return $this;
    }

    public function removeTelPersonne(Personne $telPersonne): self
    {
        $this->tel_personne->removeElement($telPersonne);

        return $this;
    }

    public function getTypeTel(): ?TypeTel
    {
        return $this->typeTel;
    }

    public function setTypeTel(?TypeTel $typeTel): self
    {
        $this->typeTel = $typeTel;

        return $this;
    }

    public function __toString()
    {
        if ($this->getNumeroTel() != null){
            return $this->getNumeroTel();
        } else {
            return "";
        }
    }
}
