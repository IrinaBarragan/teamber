<?php

namespace App\Entity;

use App\Repository\MailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=MailRepository::class)
 */
class Mail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_mail;

    /**
     * @ORM\ManyToMany(targetEntity=Societe::class, inversedBy="mails_societe")
     */
    private $mail_societe;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, inversedBy="mails_personne",cascade={"persist"})
     */
    private $mail_personne;

    /**
     * @ORM\ManyToOne(targetEntity=TypeMail::class, inversedBy="mail_type", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeMail;

    public function __construct()
    {
        $this->mail_societe = new ArrayCollection();
        $this->mail_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresseMail(): ?string
    {
        return $this->adresse_mail;
    }

    public function setAdresseMail(?string $adresse_mail): self
    {
        $this->adresse_mail = $adresse_mail;

        return $this;
    }

    /**
     * @return Collection|Societe[]
     */
    public function getMailSociete(): Collection
    {
        return $this->mail_societe;
    }

    public function addMailSociete(Societe $mailSociete): self
    {
        if (!$this->mail_societe->contains($mailSociete)) {
            $this->mail_societe[] = $mailSociete;
        }

        return $this;
    }

    public function removeMailSociete(Societe $mailSociete): self
    {
        $this->mail_societe->removeElement($mailSociete);

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getMailPersonne(): Collection
    {
        return $this->mail_personne;
    }

    public function addMailPersonne(Personne $mailPersonne): self
    {
        if (!$this->mail_personne->contains($mailPersonne)) {
            $this->mail_personne[] = $mailPersonne;
        }

        return $this;
    }

    public function removeMailPersonne(Personne $mailPersonne): self
    {
        $this->mail_personne->removeElement($mailPersonne);

        return $this;
    }

    public function getTypeMail(): ?TypeMail
    {
        return $this->typeMail;
    }

    public function setTypeMail(?TypeMail $typeMail): self
    {
        $this->typeMail = $typeMail;

        return $this;
    }
}
