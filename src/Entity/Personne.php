<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonneRepository::class)
 */
class Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_personne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom_personne;

    /**
     * @ORM\ManyToMany(targetEntity=Societe::class, inversedBy="personnes_societe", cascade={"persist"})
     */
    private $societe_personne;

    /**
     * @ORM\ManyToMany(targetEntity=Emploi::class, mappedBy="emploi_personne")
     */
    private $emplois_personne;

    /**
     * @ORM\ManyToMany(targetEntity=Adresse::class, mappedBy="adresse_personne")
     */
    private $adresses_personne;

    /**
     * @ORM\ManyToMany(targetEntity=Tel::class, mappedBy="tel_personne")
     */
    private $tels_personne;

    /**
     * @ORM\ManyToMany(targetEntity=Mail::class, mappedBy="mail_personne")
     */
    private $mails_personne;

    public function __construct()
    {
        $this->societe_personne = new ArrayCollection();
        $this->emplois_personne = new ArrayCollection();
        $this->adresses_personne = new ArrayCollection();
        $this->tels_personne = new ArrayCollection();
        $this->mails_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPersonne(): ?string
    {
        return $this->nom_personne;
    }

    public function setNomPersonne(string $nom_personne): self
    {
        $this->nom_personne = $nom_personne;

        return $this;
    }

    public function getPrenomPersonne(): ?string
    {
        return $this->prenom_personne;
    }

    public function setPrenomPersonne(string $prenom_personne): self
    {
        $this->prenom_personne = $prenom_personne;

        return $this;
    }

    /**
     * @return Collection|Societe[]
     */
    public function getSocietePersonne(): Collection
    {
        return $this->societe_personne;
    }

    public function addSocietePersonne(Societe $societePersonne): self
    {
        if (!$this->societe_personne->contains($societePersonne)) {
            $this->societe_personne[] = $societePersonne;
        }

        return $this;
    }

    public function removeSocietePersonne(Societe $societePersonne): self
    {
        $this->societe_personne->removeElement($societePersonne);

        return $this;
    }

    /**
     * @return Collection|Emploi[]
     */
    public function getEmploisPersonne(): Collection
    {
        return $this->emplois_personne;
    }

    public function addEmploisPersonne(Emploi $emploisPersonne): self
    {
        if (!$this->emplois_personne->contains($emploisPersonne)) {
            $this->emplois_personne[] = $emploisPersonne;
            $emploisPersonne->addEmploiPersonne($this);
        }

        return $this;
    }

    public function removeEmploisPersonne(Emploi $emploisPersonne): self
    {
        if ($this->emplois_personne->removeElement($emploisPersonne)) {
            $emploisPersonne->removeEmploiPersonne($this);
        }

        return $this;
    }

    /**
     * @return Collection|Adresse[]
     */
    public function getAdressesPersonne(): Collection
    {
        return $this->adresses_personne;
    }

    public function addAdressesPersonne(Adresse $adressesPersonne): self
    {
        if (!$this->adresses_personne->contains($adressesPersonne)) {
            $this->adresses_personne[] = $adressesPersonne;
            $adressesPersonne->addAdressePersonne($this);
        }

        return $this;
    }

    public function removeAdressesPersonne(Adresse $adressesPersonne): self
    {
        if ($this->adresses_personne->removeElement($adressesPersonne)) {
            $adressesPersonne->removeAdressePersonne($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tel[]
     */
    public function getTelsPersonne(): Collection
    {
        return $this->tels_personne;
    }

    public function addTelsPersonne(Tel $telsPersonne): self
    {
        if (!$this->tels_personne->contains($telsPersonne)) {
            $this->tels_personne[] = $telsPersonne;
            $telsPersonne->addTelPersonne($this);
        }

        return $this;
    }

    public function removeTelsPersonne(Tel $telsPersonne): self
    {
        if ($this->tels_personne->removeElement($telsPersonne)) {
            $telsPersonne->removeTelPersonne($this);
        }

        return $this;
    }

    /**
     * @return Collection|Mail[]
     */
    public function getMailsPersonne(): Collection
    {
        return $this->mails_personne;
    }

    public function addMailsPersonne(Mail $mailsPersonne): self
    {
        if (!$this->mails_personne->contains($mailsPersonne)) {
            $this->mails_personne[] = $mailsPersonne;
            $mailsPersonne->addMailPersonne($this);
        }

        return $this;
    }


}
