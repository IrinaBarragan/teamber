<?php

namespace App\Entity;

use App\Repository\EmploiRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmploiRepository::class)
 */
class Emploi
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $emploi;

    /**
     * @ORM\ManyToMany(targetEntity=Personne::class, inversedBy="emplois_personne")
     */
    private $emploi_personne;

    public function __construct()
    {
        $this->emploi_personne = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmploi(): ?string
    {
        return $this->emploi;
    }

    public function setEmploi(string $emploi): self
    {
        $this->emploi = $emploi;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getEmploiPersonne(): Collection
    {
        return $this->emploi_personne;
    }

    public function addEmploiPersonne(Personne $emploiPersonne): self
    {
        if (!$this->emploi_personne->contains($emploiPersonne)) {
            $this->emploi_personne[] = $emploiPersonne;
        }

        return $this;
    }

    public function removeEmploiPersonne(Personne $emploiPersonne): self
    {
        $this->emploi_personne->removeElement($emploiPersonne);

        return $this;
    }
}
