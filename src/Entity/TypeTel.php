<?php

namespace App\Entity;

use App\Repository\TypeTelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeTelRepository::class)
 */
class TypeTel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type_tel;

    /**
     * @ORM\OneToMany(targetEntity=Tel::class, mappedBy="typeTel")
     */
    private $tel_type;

    public function __construct()
    {
        $this->tel_type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeTel(): ?string
    {
        return $this->type_tel;
    }

    public function setTypeTel(string $type_tel): self
    {
        $this->type_tel = $type_tel;

        return $this;
    }

    /**
     * @return Collection|Tel[]
     */
    public function getTelType(): Collection
    {
        return $this->tel_type;
    }

    public function addTelType(Tel $telType): self
    {
        if (!$this->tel_type->contains($telType)) {
            $this->tel_type[] = $telType;
            $telType->setTypeTel($this);
        }

        return $this;
    }

    public function removeTelType(Tel $telType): self
    {
        if ($this->tel_type->removeElement($telType)) {
            // set the owning side to null (unless already changed)
            if ($telType->getTypeTel() === $this) {
                $telType->setTypeTel(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        if ($this->getTypeTel() != null){
            return $this->getTypeTel();
        } else {
            return "";
        }
   
    }
}
