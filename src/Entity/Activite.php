<?php

namespace App\Entity;

use App\Repository\ActiviteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActiviteRepository::class)
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $activite;

    /**
     * @ORM\ManyToMany(targetEntity=Societe::class, inversedBy="activites")
     */
    private $activite_societe;

    public function __construct()
    {
        $this->activite_societe = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActivite(): ?string
    {
        return $this->activite;
    }

    public function setActivite(string $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    /**
     * @return Collection|Societe[]
     */
    public function getActiviteSociete(): Collection
    {
        return $this->activite_societe;
    }

    public function addActiviteSociete(Societe $activiteSociete): self
    {
        if (!$this->activite_societe->contains($activiteSociete)) {
            $this->activite_societe[] = $activiteSociete;
        }

        return $this;
    }

    public function removeActiviteSociete(Societe $activiteSociete): self
    {
        $this->activite_societe->removeElement($activiteSociete);

        return $this;
    }
}
