<?php

namespace App\Form;

use App\Entity\TypeTel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TypeTelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            // 'type_tel', EntityType::class, array(
            //     'class'=>'App\Entity\TypeTel',
            //     'choice_label'=>'type_tel',
            //     'expanded'=>false,
            //     'multiple'=>false,
            //     // 'data' => $options['data']->getTypeTel()
            // ));

            'type_tel', ChoiceType::class, [
                'label'=> 'type telephone',
                'choices' => [
                    'administration'=>'administration',
                    'comptabilite'=>'comptabilite',
                    'managers'=>'managers',
                    'it'=>'it',
                'autre'=>'autre',
                ]]);
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeTel::class,
        ]);
    }
}
