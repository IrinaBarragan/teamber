<?php

namespace App\Form;

use App\Entity\TypeAdresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Choice;

class TypeAdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
      
        'type_adresse', ChoiceType::class, [
            'choices' => [
            'general'=>'general',
            'personnel'=>'courier',
            'facture'=>'facture',
            'livraison'=>'livraison',
            'autre'=>'autre'],
            ],
        );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeAdresse::class,
        ]);
    }
}
