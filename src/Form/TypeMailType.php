<?php

namespace App\Form;

use App\Entity\TypeMail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TypeMailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type_mail', ChoiceType::class, [
            // 'type_mail',EntityType::class, [
            //     'class'=>'App\Entity\TypeMail',
            //     'choice_label'=>'type_mail',
            //     'expanded'=>false,
            //     'multiple'=>false,
            //     // 'data' => $options['data']->getTypeMail()

               
            'choices' => [
                'administration'=>'administration',
                'comptabilite'=>'comptabilite',
                'managers'=>'managers',
                'it'=>'it',
                'autre'=>'autre',
                ]]);
        }
            

          
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeMail::class,
        ]);
    }

    
}
