<?php

namespace App\Form;

use App\Entity\Societe;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class Societe1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom_societe',
             EntityType::class, array(
                'class'=>'App\Entity\Societe',
                'choice_label'=>'nom_societe',
                'expanded'=>false,
                'multiple'=>false,
                'method'=>'post'
            ));
            //->add('logo')
            // ->add('siren')
            // ->add('activites')
            // ->add('personnes_societe')
            // ->add('adresses_societe')
            // ->add('tels_societe')
            // ->add('mails_societe')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Societe::class,
        ]);
    }
}
