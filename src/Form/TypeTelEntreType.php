<?php

namespace App\Form;

use App\Entity\TypeTel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TypeTelEntreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type_tel', TextType::class, [
                'method'=>'post',
                'data' => 'ex: service 24h',
                'label' => 'autre type'
                ]);
       
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeTel::class,
        ]);
    }
}
