<?php

namespace App\Controller;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\Societe;
use App\Entity\Activite;
use App\Entity\Tel;
use App\Entity\Mail;
use App\Entity\Emploi;
use App\Entity\Adresse;
use App\Entity\Personne;
use App\Entity\ViewEntreprise;
use App\Repository\SocieteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Length;

class RechercheEffectueeController extends AbstractController
{

    #[Route('/rechercheEffectuee', name: 'rechercheEffectuee')]
    public function search(EntityManagerInterface $em): Response
    {
        //on récupère la recherche de l'utilisateur
        $Societe = $_GET['rechercheSociete'];
        $DomaineActivite = $_GET['rechercheActivite'];
        $Mail = $_GET['rechercheMail'];
        $Telephone = $_GET['rechercheTel'];
        $Pays = $_GET['recherchePays'];
        $Ville = $_GET['rechercheVille'];
        $Nom = $_GET['rechercheNom'];
        $Prenom = $_GET['recherchePrenom'];
        $Statut = $_GET['rechercheStatut'];

        if ($Societe != '') {
            $objSociete = (object) array('Valeur' => $Societe, 'Table' => 'nom_societe');
        } else {
            $objSociete = '';
        }
        if ($DomaineActivite != '') {
            $objDomaineActivite = (object) array('Valeur' => $DomaineActivite, 'Table' => 'activite');
        } else {
            $objDomaineActivite = '';
        }
        if ($Mail != '') {
            $objMail = (object) array('Valeur' =>  $Mail, 'Table' => 'adresse_mail');
        } else {
            $objMail = '';
        }
        if ($Telephone != '') {
            $objTelephone = (object) array('Valeur' =>  $Telephone, 'Table' => 'numero_tel');
        } else {
            $objTelephone = '';
        }
        if ($Ville != '') {
            $objVille = (object) array('Valeur' =>  $Ville, 'Table' => 'ville');
        } else {
            $objVille = '';
        }
        if ($Nom != '') {
            $objNom = (object) array('personne' =>  $Nom);
        } else {
            $objNom = '';
        }
        if ($Prenom != '') {
            $objPrenom = (object) array('personne' =>  $Prenom);
        } else {
            $objPrenom = '';
        }
        if ($Statut != '') {
            $objStatut = (object) array('emploi' =>  $Statut);
        } else {
            $objStatut = '';
        }

        // on choppe les get qui sont utilisés
        $res = $this->recupereGetUtilise($objSociete, $objDomaineActivite, $objMail, $objTelephone, $objVille, $objNom, $objPrenom, $objStatut);
        // dd(count($res));

        // declaration des variables de resultats
        $resultatSociete = [];
        $resultatNomPersonne = [];
        $resultatPrenomPersonne = [];
        $resultatMail = [];
        $resultatVille = [];
        $resultatTel = [];
        $resultatActivite = [];
        $resultatEmploi = [];
        $resultatJointure = [];
        //condition de recherche pour savoir quelle fonction lancer
        //recherche par nom
        if (count($res) >= 2) {
            $resultatJointure = $this->searchWithJointure($em, $res);
        } else {
            if ($Nom != '') {
                $resultatNomPersonne = $this->searchPersonne($em, $Nom);
            }
            //recherche par prénom
            if ($Prenom != '') {
                $resultatPrenomPersonne = $this->searchPersonnePrenom($em, $Prenom);
            }
            //recherche par société
            if ($Societe != '') {
                $resultatSociete = $this->searchEntreprise($em, $Societe);
            }
            // recherche par ville
            if ($Ville != '') {
                $resultatVille = $this->searchWithVille($em, $Ville);
            }
            //recherche par mail 
            if ($Mail != '') {
                $resultatMail = $this->searchWithMail($em, $Mail);
            }
            //recherche par tel
            if ($Telephone != '') {
                $resultatTel = $this->searchWithTel($em, $Telephone);
            }
            //recherche par activite
            if ($DomaineActivite != '') {
                $resultatActivite = $this->searchWithActivite($em, $DomaineActivite);
            }
            //recherche par emploi
            if ($Statut != '') {
                $resultatEmploi = $this->searchWithEmploi($em, $Statut);
            }
        }
        // dd($resultatMailPersonne->get);

        // affiche la vue
        //  dd($resultatJointure);
         
        return $this->render('home/RechercheEffectuee.html.twig', [
            'Societes' => $resultatSociete,
            'Personnes' => $resultatNomPersonne,
            'PersonnesPrenom' => $resultatPrenomPersonne,
            'ResultatsMail' => $resultatMail,
            // 'SocietesMail' => $resultatMailSociete,
            'ResultatsVille' => $resultatVille,
            'ResultatsTel' => $resultatTel,
            'ResultatsActivite' => $resultatActivite,
            'ResultatsEmploi' => $resultatEmploi,
            'ResultatsJointureEntreprise'=> $resultatJointure,
        ]);
    }



    //fonction recupère par nom Societe
    private function searchEntreprise(EntityManagerInterface $em,  $societe)
    {
        //recherche par nom societe
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('u')
            ->from(societe::class, 'u')
            ->where('u.nom_societe LIKE :search')
            ->setParameter('search', '%' . $societe . '%');
        $resultatSociete = $queryBuilder->getQuery()->getResult();
        return $resultatSociete;
    }



    // Fonction récupère par Nom personne
    private function searchPersonne(EntityManagerInterface $em, $nom)
    {
        //recherche par  nom personne
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(personne::class, 'a')
            ->where('a.nom_personne LIKE :search')
            ->setParameter('search', '%' . $nom . '%');
        $resultatNomPersonne = $queryBuilder2->getQuery()->getResult();
        return $resultatNomPersonne;
    }


    // Fonction récupère par Prénom personne
    private function searchPersonnePrenom(EntityManagerInterface $em, $prenom)
    {
        //recherche par  nom personne
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(personne::class, 'a')
            ->where('a.prenom_personne LIKE :search')
            ->setParameter('search', '%' . $prenom . '%');
        $resultatPrenomPersonne = $queryBuilder2->getQuery()->getResult();
        return $resultatPrenomPersonne;
    }

    // Fonction récupère Personne par mail 
    private function searchwithMail(EntityManagerInterface $em, $mail)
    {
        //recherche par  mail  entreprise
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(mail::class, 'a')
            ->where('a.adresse_mail LIKE :search')
            ->setParameter('search', '%' . $mail . '%');
        $resultatMail = $queryBuilder2->getQuery()->getResult();
        return $resultatMail;
    }



    // Fonction récupère  par ville 
    private function searchWithVille(EntityManagerInterface $em, $ville)
    {
        //recherche par  Ville
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(adresse::class, 'a')
            ->where('a.ville LIKE :search')
            ->setParameter('search', '%' . $ville . '%');
        $resultatVille = $queryBuilder2->getQuery()->getResult();
        return $resultatVille;
    }


    // Fonction récupère  par tel 
    private function searchWithTel(EntityManagerInterface $em, $tel)
    {
        //recherche par  tel
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(tel::class, 'a')
            ->where('a.numero_tel LIKE :search')
            ->setParameter('search', '%' . $tel . '%');
        $resultatTel = $queryBuilder2->getQuery()->getResult();
        return $resultatTel;
    }


    // Fonction récupère  par activite 
    private function searchWithActivite(EntityManagerInterface $em, $activite)
    {
        //recherche par  tel
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(activite::class, 'a')
            ->where('a.activite LIKE :search')
            ->setParameter('search', '%' . $activite . '%');
        $resultatActivite = $queryBuilder2->getQuery()->getResult();
        return $resultatActivite;
    }

    // Fonction récupère  par emploi
    private function searchWithEmploi(EntityManagerInterface $em, $emploi)
    {
        //recherche par  emploi
        $queryBuilder2 = $em->createQueryBuilder();
        $queryBuilder2->select('a')
            ->from(emploi::class, 'a')
            ->where('a.emploi LIKE :search')
            ->setParameter('search', '%' . $emploi . '%');
        $resultatEmploi = $queryBuilder2->getQuery()->getResult();
        return $resultatEmploi;
    }

    // Fonction récupère jointure
    private function searchWithJointure(EntityManagerInterface $em, $tableauGet)
    {   
        //recherche par  emploi

        
        $rsm = new ResultSetMapping();
         $rsm->addEntityResult('App\Entity\ViewEntreprise', 'a');
         $rsm->addFieldResult('a','nom_societe', 'nom_societe');
         $rsm->addFieldResult('a','logo', 'logo');
         $rsm->addFieldResult('a','siren', 'siren');
         $rsm->addFieldResult('a','activite', 'activite');
         $rsm->addFieldResult('a','numero_tel', 'numero_tel');
         $rsm->addFieldResult('a','type_tel', 'type_tel');
         $rsm->addFieldResult('a','adresse_mail', 'adresse_mail');
         $rsm->addFieldResult('a','type_mail', 'type_mail');
         $rsm->addFieldResult('a','voie', 'voie');
         $rsm->addFieldResult('a','code_postal', 'code_postal');
         $rsm->addFieldResult('a','ville', 'ville');
         $rsm->addFieldResult('a','type_adresse', 'type_adresse');
        //  $rsm->addFieldResult('a','siret', 'siret');
         
        $table0= $tableauGet[0]->Table ;
        $table1 = $tableauGet[1]->Table;
        // dd($tableauGet[0]->Valeur);

        //compte le nombre de parametre envoyer par le formulaire
        $compteur = (count($tableauGet));

        $requeteSQL =  'SELECT * 
        FROM viewEntreprise 
        WHERE '.$table0.' LIKE "%'.$tableauGet[0]->Valeur.'%" 
        AND '.$table1.' LIKE "%'.$tableauGet[1]->Valeur.'%"';

        //on ajoute les where a la requete  en fonction du nombre de params
        if ($compteur > 2) 
        {
            for ($t = 2; $t < $compteur; $t++) {
                
                $requeteSQL = $requeteSQL.'AND '.$tableauGet[$t]->Table.' LIKE "%'.$tableauGet[$t]->Valeur.'%"';
                //array_push($tabrequete, 'AND '.$tableauGet[$t]->Table.' LIKE "%'.$tableauGet[$t]->Valeur.'%"');
            }

            //dd($requeteSQL);
        }
            
        $query = $em->createNativeQuery(
            $requeteSQL
            , $rsm );
            
        $resultatJointure = $query->getScalarResult();
        // dd($resultatJointure);
        return $resultatJointure;
    }



    private function recupereGetUtilise($societe, $domaineActivite, $mail, $telephone, $ville, $nom, $prenom, $statut)
    {
        $GetUtilise = [];
        $getUtiliseResultat = [];
        array_push($GetUtilise, $societe, $domaineActivite, $mail, $telephone, $ville, $nom, $prenom, $statut);
        foreach ($GetUtilise as $GetUtilise) {
            if ($GetUtilise != '') {

                array_push($getUtiliseResultat, $GetUtilise);
            }
        }
        return $getUtiliseResultat;
    }
}
