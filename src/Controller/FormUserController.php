<?php

namespace App\Controller;

use App\Form\Societe1Type;
use App\Form\SocieteType;
use App\Form\ActiviteType;
use App\Form\AdresseUserType;
use App\Form\TypeAdresseUserType;
use App\Form\TelephonType;
use App\Form\TypeTelUserType;
use App\Form\PersonneType;
use App\Form\TypeMailUserType;
use App\Form\MailType;
use App\Form\EmploiType;
use App\Form\EmploiNType;

use App\Entity\Societe;
use App\Entity\Activite;
use App\Entity\Adresse;
use App\Entity\TypeAdresse;
use App\Entity\Tel;
use App\Entity\Mail;
use App\Entity\TypeMail;
use App\Entity\TypeTel;
use App\Entity\Personne;
use App\Entity\Emploi;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class FormUserController extends AbstractController
{
    #[Route('/form/user', name: 'form_user')]
    public function index(Request $request): Response
    {
        if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['Entreprise']))
        {
          
           return $this->redirectToRoute('form_entr', [
                'controller_name' => 'FormUserController',
            ]);
        }

        $ent = new Societe;
        $emp = new Emploi;
       // $empN =  new Emploi;
        $adr = new Adresse;
        $typeadr = new TypeAdresse;
        $user = new Personne;
        $telef = new Tel;
        $typetel = new TypeTel;
        $mail = new Mail;
        $typemail = new TypeMail;


        $form1 = $this->createForm(Societe1Type::class, $ent);
        $form3 = $this->createForm(EmploiType::class, $emp);
       // $formN = $this->createForm(EmploiNType::class, $empN);
        $form4 = $this->createForm(AdresseUserType::class, $adr);
        $form5 = $this->createForm(TypeAdresseUserType::class, $typeadr);

        $form6 = $this->createForm(PersonneType::class, $user);
        $form7 = $this->createForm(TelephonType::class, $telef);
        $form8 = $this->createForm(TypeTelUserType::class, $typetel);
        $form9 = $this->createForm(MailType::class, $mail);
        $form10 = $this->createForm(TypeMailUserType::class, $typemail);

        $form1->handleRequest($request);
        $form3->handleRequest($request);
      //  $formN->handleRequest($request);
        $form4->handleRequest($request);
        $form5->handleRequest($request);
        $form6->handleRequest($request);
        $form7->handleRequest($request);
        $form8->handleRequest($request);
        $form9->handleRequest($request);
        $form10->handleRequest($request);

        if (($form1->isSubmitted()) &&($form3->isSubmitted()) && ($form4->isSubmitted()) && ($form5->isSubmitted()) && ($form6->isSubmitted()) && ($form7->isSubmitted()) && ($form8->isSubmitted()) && ($form9->isSubmitted()) && ($form10->isSubmitted())) {
            $n = isset($_POST['type_adresse']) ? $_POST['type_adresse'] : false;
            $ent->getNomSociete();
           
           $repoEnt = $this->getDoctrine()->getRepository(Societe::class);
           $e=$repoEnt->findOneBy(['nom_societe'=>$ent->getNomSociete()]);
//dd($e);
           if($e){
               $e->addPersonnesSociete($user);

           }

            $repoTypeTel = $this->getDoctrine()->getRepository(TypeTel::class);
         
            $tt = $repoTypeTel->findOneBy(['type_tel' => 'personnel']);
            if ($tt){
            $tt->addTelType($telef);
            }else{
                $typetel->addTelType(($telef));
            }

            $repoTypeAdresse = $this->getDoctrine()->getRepository(TypeAdresse::class);
            $ta = $repoTypeAdresse->findOneBy(['type_adresse' => 'personnel']);
            if($ta){
                $ta->addAdresseType($adr);
            }else{
                $typeadr->addAdresseType($adr);
            }
            

            $repoTypeMail = $this->getDoctrine()->getRepository(TypeMail::class);
            $tm = $repoTypeMail->findOneBy(['type_mail' => 'personnel']);
            if($tm){
            $tm->addMailType($mail);
            }else{
                $typemail->addMailType($mail);
            }
           
          
            $emp->addEmploiPersonne($user);      
            $adr->addAdressePersonne($user);
            $telef->addTelPersonne($user);
          //  $user->addSocietePersonne($ent);
            $mail->addMailPersonne($user);


            $entityManager = $this->getDoctrine()->getManager();
           // $entityManager->persist($ent);
           $entityManager->persist($emp);
            $entityManager->persist($user);

            $entityManager->persist($adr);
            
            $entityManager->persist($telef);
  
            $entityManager->persist($mail);
         
            $entityManager->flush();
            return new Response('Personne est bien enregistré!');
        } else {

            return $this->render('form_user/index.html.twig', [
                'form1' => $form1->createView(),
                'form3' => $form3->createView(),
                'form4' => $form4->createView(),
                'form5' => $form5->createView(),
                'form6' => $form6->createView(),
                'form7' => $form7->createView(),
                'form8' => $form8->createView(),
                'form9' => $form9->createView(),
                'form10' => $form10->createView(),
                //'formN'=>$formN->createView(),


            ]);

        }}}
       


