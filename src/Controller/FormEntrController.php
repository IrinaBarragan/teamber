<?php

namespace App\Controller;

use App\Form\SocieteType;
use App\Form\ActiviteType;
use App\Form\AdresseType;
use App\Form\TypeAdresseType;
use App\Form\TypeAdresseEntrType;
use App\Form\TelephonType;
use App\Form\TypeTelType;
use App\Form\PersonneType;
use App\Form\TypeMailType;
use App\Form\TypeMailEntrType;
use App\Form\TypeTelEntreType;
use App\Form\MailType;
use App\Form\EmploiType;

use App\Entity\Societe;
use App\Entity\Activite;
use App\Entity\Adresse;
use App\Entity\TypeAdresse;
use App\Entity\Tel;
use App\Entity\Mail;
use App\Entity\TypeMail;
use App\Entity\TypeTel;
use App\Entity\Personne;
use App\Entity\Emploi;
use Doctrine\Common\Collections\Expr\Value;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FormEntrController extends AbstractController
{
  #[Route('/form/entr', name: 'form_entr')]
  public function index(Request $request): Response
  {



    $ent = new Societe;
    $act = new Activite;
    $adr = new Adresse;
    $typeadr = new TypeAdresse;

    $typeadr51 = new TypeAdresse;
    $telef = new Tel;
    $typetel = new TypeTel;

    $typetel81 = new TypeTel;
    $mail = new Mail;
    $typemail = new TypeMail;
    $typemail101 = new TypeMail;

    $form1 = $this->createForm(SocieteType::class, $ent);
    $form2 = $this->createForm(ActiviteType::class, $act);
    $form4 = $this->createForm(AdresseType::class, $adr);
    $form5 = $this->createForm(TypeAdresseType::class, $typeadr);
    $form51 = $this->createForm(TypeAdresseEntrType::class, $typeadr51);
    $form7 = $this->createForm(TelephonType::class, $telef);
    $form8 = $this->createForm(TypeTelType::class, $typetel);
    $form81 = $this->createForm(TypeTelEntreType::class, $typetel81);
    $form9 = $this->createForm(MailType::class, $mail);
    $form10 = $this->createForm(TypeMailType::class, $typemail);
    $form101 = $this->createForm(TypeMailEntrType::class, $typemail101);

    $form1->handleRequest($request);
    $form2->handleRequest($request);
    $form4->handleRequest($request);
    $form5->handleRequest($request);
    $form51->handleRequest($request);
    $form7->handleRequest($request);
    $form8->handleRequest($request);
    $form81->handleRequest($request);
    $form9->handleRequest($request);
    $form10->handleRequest($request);
    $form101->handleRequest($request);



    if (($form1->isSubmitted()) && ($form2->isSubmitted()) && ($form4->isSubmitted()) && ($form5->isSubmitted()) && ($form9->isSubmitted()) && ($form10->isSubmitted()) && ($form7->isSubmitted()) && ($form8->isSubmitted())) {

      $option5 = isset($_POST['type_adresse']) ? $_POST['type_adresse'] : false;
      $option8 = isset($_POST['type_tel']) ? $_POST['type_tel'] : false;
      $option10 = isset($_POST['type_mail']) ? $_POST['type_mail'] : false;
      //dd($option5, $option8, $option10);

      $adr->addAdresseSociete($ent);
      $act->addActiviteSociete($ent);
      $telef->addTelSociete($ent);
      $mail->addMailSociete($ent);

      if ($option8['type_tel'] == 'autre') {


        $typetel81->addTelType($telef);
      } else {
        $repoTypeTel = $this->getDoctrine()->getRepository(TypeTel::class);
        // dd($option8);
        $tt = $repoTypeTel->findOneBy(['type_tel' => $option8['type_tel']]);
        if (!$tt) {
          $typetel->addTelType($telef);
        } else {
          $tt->addTelType($telef);
        }
      }


      if ($option5['type_adresse'] == 'autre') {

        $typeadr51->addAdresseType($adr);
      } else {

        $repoTypeAdresse = $this->getDoctrine()->getRepository(TypeAdresse::class);
        $ta = $repoTypeAdresse->findOneBy(['type_adresse' => $option5['type_adresse']]);

        if (!$ta) {

          $typeadr->addAdresseType($adr);
        } else {
          $ta->addAdresseType($adr);
        }
      }



      if ($option10['type_mail'] == 'autre') {

        $typemail101->addMailType($mail);
      } else {
        $repoMailType = $this->getDoctrine()->getRepository(TypeMail::class);
        // dd($option8);
        $tm = $repoMailType->findOneBy(['type_mail' => $option10['type_mail']]);
        if (!$tm) {
          $typemail->addMailType($mail);
        } else {
          $tm->addMailType($mail);
        }
      }




      //enregistrement en base donne

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($ent);
      $entityManager->persist($act);

      if ($option5['type_adresse'] == 'autre') {
        $entityManager->persist($typeadr51);
        $entityManager->persist($adr);
      } else {
        $entityManager->persist($adr);
      };

      if ($option8['type_tel'] == 'autre') {
        $entityManager->persist($typetel81);
        $entityManager->persist($telef);
      } else {
        $entityManager->persist($telef);
      };

      if ($option10['type_mail'] == 'autre') {
        $entityManager->persist($typemail101);
        $entityManager->persist($mail);
      } else {
        $entityManager->persist($mail);
      };




      $entityManager->flush();
      return new Response('Entreprise est bien enregistré!');
    } else {

      return $this->render('form_entr/index.html.twig', [
        'form1' => $form1->createView(),
        'form2' => $form2->createView(),
        'form4' => $form4->createView(),
        'form5' => $form5->createView(),
        'form51' => $form51->createView(),

        'form7' => $form7->createView(),
        'form8' => $form8->createView(),
        'form81' => $form81->createView(),
        'form9' => $form9->createView(),
        'form10' => $form10->createView(),
        'form101' => $form101->createView(),
      ]);
    }
  }
}
