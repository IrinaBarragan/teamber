<?php

namespace App\Controller;

use App\Form\SocieteType;
use App\Form\ActiviteType;
use App\Form\AdresseType;
use App\Form\TypeAdresseType;
use App\Form\TelType;
use App\Form\TypeTelType;
use App\Form\PersonneType;
use App\Form\TypeMailType;
use App\Form\MailType;
use App\Form\EmploiType;

use App\Entity\Societe;
use App\Entity\Activite;
use App\Entity\Adresse;
use App\Entity\TypeAdresse;
use App\Entity\Tel;
use App\Entity\Mail;
use App\Entity\TypeMail;
use App\Entity\TypeTel;
use App\Entity\Personne;
use App\Entity\Emploi;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    #[Route('/form', name: 'form')]
   
       



    public function index(Request $request): Response
    {
        if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['Entreprise']))
    {
      
       return $this->redirectToRoute('form_entr', [
            'controller_name' => 'FormEntrController',
        ]);
    }
    else if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['Personne']))
    {
      
       return $this->redirectToRoute('form_user', [
            'controller_name' => 'FormEntrController',
        ]);
    }
    
    
    else{

    return $this->render('form/index.html.twig', [
        'controller_name' => 'FormController',
    ]);
    }





    

}
}
