<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheController extends AbstractController
{
    #[Route('/recherche', name: 'recherche')]
    public function search(): Response
    {
        return $this->render('home/Recherche.html.twig', [
            'controller_name' => 'RechercheController',
        ]);
    }
}
